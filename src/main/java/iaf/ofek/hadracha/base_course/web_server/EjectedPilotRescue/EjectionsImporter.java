package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.Coordinates;
import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Data.Entity;
import iaf.ofek.hadracha.base_course.web_server.Utilities.ListOperations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class EjectionsImporter {

    @Value("${ejections.server.url}")
    public String EJECTION_SERVER_URL;

    @Value("${ejections.namespace}")
    public String NAMESPACE;

    private final RestTemplate restTemplate;
    private final CrudDataBase dataBase;
    private final ListOperations listOperationsController;
    private static final Double FIX_LOCATION_TO_NORTH = 1.7;

    public EjectionsImporter(
            RestTemplateBuilder restTemplateBuilder,
            CrudDataBase dataBase,
            ListOperations listOperationsController) {
        restTemplate = restTemplateBuilder.build();
        this.dataBase = dataBase;
        this.listOperationsController = listOperationsController;
        ScheduledExecutorService mapUpdater = Executors.newSingleThreadScheduledExecutor();
        mapUpdater.scheduleAtFixedRate(this::updateEjections, 1, 1, TimeUnit.SECONDS);
    }

    private void updateEjections() {
        try {
            List<EjectedPilotInfo> ejectionsFromServer;
            ResponseEntity<List<EjectedPilotInfo>> ejectionsResponseEntity =
                    restTemplate.exchange(
                            EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE,
                            HttpMethod.GET,
                            null,
                            new ParameterizedTypeReference<List<EjectedPilotInfo>>() {
                            });
            ejectionsFromServer = ejectionsResponseEntity.getBody();
            if (ejectionsFromServer != null) {
                fixEjectedPilotsLocations(ejectionsFromServer);
            }
            List<EjectedPilotInfo> updatedEjections = ejectionsFromServer;
            List<EjectedPilotInfo> previousEjections = dataBase.getAllOfType(EjectedPilotInfo.class);

            addEjections(updatedEjections, previousEjections);
            deleteEjections(updatedEjections, previousEjections);

        } catch (RestClientException error) {
            System.err.println("Could not get ejections: " + error.getMessage());
            error.printStackTrace();
        }
    }

    private void fixEjectedPilotsLocations(List<EjectedPilotInfo> ejections) {
        for (EjectedPilotInfo ejectedPilotInfo : ejections) {
            ejectedPilotInfo.setCoordinates(new Coordinates(ejectedPilotInfo.getCoordinates().lat+FIX_LOCATION_TO_NORTH
                    , ejectedPilotInfo.getCoordinates().lon));

        }
    }

    private void addEjections(
            List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
        List<EjectedPilotInfo> ejectionsToAdd = editEjections(updatedEjections, previousEjections);
        ejectionsToAdd.forEach(dataBase::create);
    }

    private void deleteEjections(
            List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
        List<EjectedPilotInfo> ejectionsToRemove = editEjections(previousEjections, updatedEjections);
        ejectionsToRemove.stream()
                .map(EjectedPilotInfo::getId)
                .forEach(id -> dataBase.delete(id, EjectedPilotInfo.class));
    }

    private List<EjectedPilotInfo> editEjections(
            List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
        return listOperationsController.subtract(
                updatedEjections, previousEjections, new Entity.ByIdEqualizer<>());
    }
}
