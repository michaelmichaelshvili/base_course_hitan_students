package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.InMemoryMapDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("ejectedPilotRescue/")
public class EjectionsController {

    @Autowired
    private InMemoryMapDataBase database;

    @Autowired
    private AirplanesAllocationManager airplanesAllocationManager;

    @GetMapping("infos")
    public List<EjectedPilotInfo> getAllEjections() {
        return database.getAllOfType(EjectedPilotInfo.class);
    }

    @GetMapping("takeResponsibility")
    public void takeResponsibility(@RequestParam int ejectionId, @CookieValue("client-id") String clientId) {
        EjectedPilotInfo ejectedPilotInfo = database.getByID(ejectionId, EjectedPilotInfo.class);
        if (ejectedPilotInfo != null && ejectedPilotInfo.getRescuedBy() == null) {
            ejectedPilotInfo.setRescuedBy(clientId);
            airplanesAllocationManager.allocateAirplanesForEjection(ejectedPilotInfo, clientId);
            database.update(ejectedPilotInfo);
        }
    }

}
